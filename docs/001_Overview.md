Watch dog is an application that will have the capability to deep freeze, lock, change various settings, and issue commands remotely on an IBT (Integrated Bedside Terminal). Watch Dog will allow for commands to be issued and continue where it left off even after a restart. This will enable a set it and forget it approach when updating and changing settings on an IBT machine. 

Watch Dog provides access both remotely, locally, and future plans for connecting from a server and deploying fixes and patch updates from the cloud.


Description
-----------

Watch Dog is developed using 4.5.1 Microsoft Framework using [Visual Studio 2013](http://www.visualstudio.com/). There are going to be two components that make up the complete feature set of Watch Dog. There is the client application and the server application which will allow for a push deployment to the selected machine(s).

###Client Side
The client side of Watch Dog will allow for changes on the IBT Machine. These settings can be seen inside the [Commands section](index.php?Commands/Commands_Overview). This will contain various command settings such as informing the users information relaying any service interruption, changes or any general information. The client Side application is the core of Watch Dog. All Update and changes to the settings will have to go through the client Watch Dog application. 

Watch Dog will be able to do alot more than just providing an entrance to make settings change, it will also be the platform to protected the users from exiting the application and potential accessing restricted content.

Watch Dog will govern what command a user is allowed to enter, restricting a user from common things such as exiting the application, accessing Task Manager and Local Folders and application. This will work with Features already embedded into windows to eliminate uploading, downloading and executing files.


###Server Side Application
This will support deploying patches and updates to the client machine from behind the scene. This will provide the interface for the technition to deploy updates and make changes to IBT machine either on a individual or multitude of machines.





