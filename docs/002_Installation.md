##General
Installing Watch Dogs is relatively simple task. Follows the following steps:

1. Create a Seperate Partition for Watch Dog.

2. Create a Folder IBTWatchDog/IBTWatchdog.exe.

3. Move WatchDog into the startup folder.

4. You are ready to have Watch Dog Protected your application.


####Detailed
1. Open Disk Manager.
2. Create a New Disk Partition. 
*note that you may not have room for a partition. In this case you can shrink the current partition volume by right clicking on to it and select shrink. Once the partition is shrink you will have unallocated disk space. You can use this for the new partition*.

3. Create Give the Partition a Drive Letter. This will make it visible from **My Computer**.

4. Create a *Folder* call **IBTWatchDog**.

5. Inside the newly created folder place the **IBTWatchDog.exe** executable. 

6. You will then need to create a shortcut to this file inside your **Startup** folder. 

7. Open up your Startup folder along side the **IBTWatchDog** Folder and hold **ALT** and drag over to the **Startup* Folder.

8. You Should now have a link inside start up that will run each time the Machine is Booted.


















