###Wait [timeout]

This will trigger the executing script to pause. This can be used for things such as waiting for a driver to load or pausing after executing some other function.