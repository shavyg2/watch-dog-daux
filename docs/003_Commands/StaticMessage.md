The **StaticMessage** command will trigger a one screen display that will require a user interaction to close. This message will automatically scale to make the message as reader friendly as possible.  

If there is alot of text then the text will be scaled down to fit. If the amount of text is too big then the text will then be scrollable so that the client can scroll and read the message.  

In the event that there is very little text on the screen then the text will scale to try and take up as much screen real estate.  

The Message will require the client to close the prompt by clicking the close button.

If there is a [Message](index.php?Commands/Message) this message will be displayed above the static message so that it will be visible.