###Message [timeout]

The **Message** command will allow for a dynamic message to be posted to the screen. This message will also allow for a timeout and does not require any user interaction. This is to be used as a prompt so that you can warn a user of an imminent shutdown or any action that can disrupt there usage of the IBT.  

It is recommended to display a message to the user to let them be aware if you are going to disrupt there service.