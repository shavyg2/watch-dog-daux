|Command Names|Options|
|-------------|-----------|
|[FBWF](index.php?Commands/FBWF)||
|~~[Freeze](index.php?Commands/Freeze)~~||
|[Info](index.php?Commands/Info)||
|[Keyboard](index.php?Commands/Keyboard)|[Enable/Disable]|
|~~[Lock](index.php?Commands/Lock)~~|~~[C:\Path\To\Application.exe]~~|
|[Message](index.php?Commands/Message)|[Message][Timeout]|
|[Mouse](index.php?Commands/Mouse)|[Enable/Disable]|
|[Restart](index.php?Commands/Restart)||
|[StartUpApp](index.php?Commands/StartUpApp)|[C:\Path\To\Application.exe]|
|[StaticMessage](index.php?Commands/StaticMessage)|[Message]|
|[Status](index.php?Commands/Status)||
|[Stop](index.php?Commands/Stop)||
|[TaskManager](index.php?Commands/TaskManager)|[Enable/Disable]|