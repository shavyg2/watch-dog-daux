###StartUpApp [path_to_application,...string]
This Command allow for Watch Dog to start up an application in a lockdown mode. This stops the user from interacting with any other applications. This application typically received too strings.


1. Path to Watchdog *D:\IBTWatchDog\IBTWatchdog.exe*
2. Path to Application to watch

####Example
```
StartUpApp D:\IBTWatchDog\IBTWatchdog.exe,C:\IBT\IntegratedBedsideCompanion.exe
```