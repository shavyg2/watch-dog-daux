###Keyboard [Enable/Disable]

This command allows the client's keyboard to be enabled or disabled. This will also trigger a restart so that this can be registered into the registry. Once the IBT is restarted the keyboard will be disabled. Once the keyboard is disabled it will stay disabled even after multiple restarts.