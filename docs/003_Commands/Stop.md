###Stop

This will trigger for WatchDog to stop watching the application, however all settings will remain the same. If the mouse or keyboard was disabled they will remain disabled. This will persist even after a reboot. Inorder to enable or change settings WatchDog will need to be booted up again and the settings changed.